# ELC Take Home Test

This project tests the usage of a Gitlab CI pipeline to automate the process of building a Docker image (Alpine Linux with Ansible and Molecule installed) and running a Docker container from that image that will execute the included Ansible playbook on every commit to the project repository.

This pipeline consists of three stages, defined in the .gitlab-ci.yml file:

**build_docker** - Builds the Docker image from the dockerfile, tagged with the commit SHA and pushed to the container registry.

**deploy_docker** - Pulls the image built in the first stage and pushes it to registry, tagged as "lastest."

**run_ansible** - Runs a Docker container from the latest image, executing the playbook in the repository root (playbook.yml).


## Basic Usage

Modify the file _playbook.yml_ to include the playbook that should be run on every commit.

## Manually build the Docker image from Dockerfile and run locally

#### 1. Clone the Repository

Use `git` to clone the repository to the desired directory on the local machine.

```
cd folder/to/clone-to/
git clone https://gitlab.com/elc2/take-home-test.git
```


#### 2. Build the image from the Dockerfile

Run the following command from the root directory of the local copy of the repository to build the image.

```
docker build .
```

Alternatively, you may choose to specify a repository and tag when building the image using the -t switch. In the below example, the project repository is specifed and the image is tagged "local."

```
docker build -t registry.gitlab.com/elc2/take-home-test:local .
```

At the end of the build process, the Image ID (a 12-character unique identifier) of the new image will be outputted on the line starting "Successfully built."


#### 3. Run the Docker container locally

The [entrypoint](https://docs.docker.com/engine/reference/builder/#entrypoint) is `ansible-playbook` and by default, the included playbook (playbook.yml) will be run. The examples below uses the Image ID from Step 2 to inform Docker of the image to use. In this example, the Image ID is _7da7f1d108ec_.

```
docker run 7da7f1d108ec
```

Alternatively, the user may override the default behavior and specify a different Ansible playbook when running the Docker container. In the below example the playbook `myplaybook.yml` has been placed in the directory `playbooks` which is mounted to `/ansible/playbooks` inside the container, using the -v switch.

```
docker run -v "$(pwd)"/playbooks:/ansible/playbooks 7da7f1d108ec playbooks/myplaybook.yml
```

## Push a locally built image to the container registry

The following command can be used to push the locally built image to the project's container registry. In this example, the image to be pushed is tagged "local."

```
docker push registry.gitlab.com/elc2/take-home-test:local
```

